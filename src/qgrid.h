/**
 * @file
 * @author Luca Tavernari
*/

#ifndef QGRID_H
#define QGRID_H

#include <QWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QRect>
#include <map>
#include "grid.h"
#include <QLabel>
#include <QPushButton>


/**
 * Displays the grid and handles inputs.
*/
class QGrid : public QWidget
{
    Q_OBJECT

public:
    /**
     * Creates a new QGrid.
     */
    explicit QGrid(QWidget *parent = 0);

    /**
     * Destroys the istancied QGrid.
     */
    ~QGrid();

    /**
     * Function called whenever the update() function is called. Updates the
     * Grid's view.
     */
    void paintEvent (QPaintEvent* ev);

    /**
     * Whenever one of the 4 arrow is pressed on the keyboard, the respective
     * Grid's public function is called. It also handles the Grid's
     * Game Over status.
     *
     * @param ev indetifies one of the 4 arrow pressed
     */
    void keyPressEvent (QKeyEvent* ev);

public slots:
    /**
     * Resets the grid starting a new game.
     */
    void newGame();


private:
    std::map<int, std::pair<QColor, QColor> > _map;
    Grid* grid;
    QRect** tile;
    QLabel* label_score;
    QString label_text;
    QPushButton* button;
    bool status;
};

#endif // QGRID_H
