#include "grid.h"
#include <algorithm>
#include <QTime>

Grid::Grid()
{
    qsrand(time(NULL));


    this->m = new int*[4];
    this->score = 0;
    for (int i = 0; i < 4; i++)
        m[i] = new int[4];

    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            m[i][j] = 0;

    spawnElement();
    spawnElement();
}

Grid::~Grid(){
    for (int i = 0; i < 4; i++)
        delete m[i];
}

void Grid::reset(){
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            m[i][j] = 0;

    score = 0;
    spawnElement();
    spawnElement();
}

bool Grid::spawnElement(){
    int ri = qrand() % 4;
    int rj;

    int stopi = ri;

    bool spawned = false;

    do {
        rj = qrand() % 4;
        int stopj = rj;

        do {
            if (m[ri][rj] == 0){
                int val = qrand();

                m[ri][rj] = val % 2 == 0 ?
                            2 : 4;

                spawned = true;
            }

            rj = (rj + 1) % 4;
        } while (rj != stopj && !spawned);

        ri = (ri + 1) % 4;
    } while (ri != stopi && !spawned);

    return spawned;
}

bool Grid::move(int row, int col, bool selectorRC, int inc, bool update){
    int a[4];
    int ia = inc > 0 ? 0 : 3;

    int rowC = row;
    int colC = col;

    for (; (inc > 0 ? (selectorRC ? col < 4 : row < 4) : (selectorRC ? col >= 0 : row >= 0));
         (selectorRC ? col += inc : row += inc)) {

        if (m[row][col] != 0){
            a[ia] = m[row][col];
            ia += inc;
        }
    }



    for (; (inc > 0 ? ia < 4 : ia >= 0); ia += inc)
        a[ia] = 0;

    bool diff = false;

    for (; (inc > 0 ? (selectorRC ? colC < 4 : rowC < 4) : (selectorRC ? colC >= 0 : rowC >= 0));
         (selectorRC ? colC += inc : rowC += inc)) {

        if (m[rowC][colC] != a[selectorRC ? colC : rowC]){
            diff = true;

            if(!update)
                return diff;
        }

        if (update)
            m[rowC][colC] = a[selectorRC ? colC : rowC];
    }

    return diff;
}

bool Grid::merge(int row, int col, bool selectorRC, int inc, bool update){
    bool diff = false;

    for (; (inc > 0 ? (selectorRC ? col < 3 : row < 3) : (selectorRC ? col > 0 : row > 0));
         (selectorRC ? col += inc : row += inc)) {
        if (m[row][col] == (selectorRC ? m[row][col + inc] : m[row + inc][col])){
            diff = true;
            if (update){

                m[row][col] += m[row][col];
                score += m[row][col];

                if (selectorRC)
                    m[row][col + inc] = 0;
                else
                    m[row + inc][col] = 0;
            }

            else if (diff && !update)
                return diff;
        }
    }

    return diff;
}

bool Grid::iterateRC(int row, int col, bool selectorRC, int inc, bool update){
    bool need2spawn = false;

    for(; (selectorRC ? row < 4 : col < 4);
        (selectorRC ? row ++ : col ++)) {
        bool ret;
        ret = move (row, col, selectorRC, inc, update);
        need2spawn = need2spawn || ret;
        ret = merge (row, col, selectorRC, inc, update);
        need2spawn = need2spawn || ret;
        ret = move (row, col, selectorRC, inc, update);
        need2spawn = need2spawn || ret;
    }

    if (need2spawn)
        spawnElement();

    return need2spawn;
}

bool Grid::gameStatus(){
    bool status = iterateRC (0, 0, false, 1, false);

    bool ret = iterateRC (3, 0, false, -1, false);
    status = status || ret;

    ret = iterateRC (0, 0, true, 1, false);
    status = status || ret;

    ret = iterateRC (0, 3, true, -1, false);
    status = status || ret;

    return status;
}

bool Grid::up(){
    return iterateRC(0, 0, false, 1);
}

bool Grid::down(){
    return iterateRC (3, 0, false, -1);
}

bool Grid::left(){
    return iterateRC (0, 0, true, 1);
}

bool Grid::right(){
    return iterateRC (0, 3, true, -1);
}

int Grid::getScore(){
    return score;
}

int Grid::getTileValue(int i, int j){
    return m[i][j];
}
