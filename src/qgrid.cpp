#include "qgrid.h"
#include <QFont>

QGrid::QGrid(QWidget *parent) :
    QWidget(parent)
{
    this->setWindowTitle("2048");

    this->setFixedSize(620,620);

    grid = new Grid();

    status = true;

    if (_map.empty()) {
        _map.insert(std::pair<int,std::pair<QColor, QColor> >
                     (0,std::pair<QColor, QColor>(QColor("#b8b6b6"),QColor("#615e5d"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (2,std::pair<QColor, QColor>(QColor("#f5f5f5"), QColor("#615e5d"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (4, std::pair<QColor, QColor>(QColor("#f5f5dc"), QColor("#615e5d"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (8, std::pair<QColor, QColor>(QColor("#ffa07a"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (16, std::pair<QColor, QColor>(QColor("#ff7f50"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (32, std::pair<QColor, QColor>(QColor("#ff6347"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (64, std::pair<QColor, QColor>(QColor("#ff0000"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (128, std::pair<QColor, QColor>(QColor("#fff060"), QColor("#615e5d"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (256, std::pair<QColor, QColor>(QColor("#f0e050"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (512, std::pair<QColor, QColor>(QColor("#f0e010"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (1024, std::pair<QColor, QColor>(QColor("#f0d000"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (2048, std::pair<QColor, QColor>(QColor("#f0c000"), QColor("#ffffff"))));

        _map.insert(std::pair<int, std::pair<QColor, QColor> >
                     (4096, std::pair<QColor, QColor>(QColor("#000000"), QColor("#ffffff"))));
    }

    tile = new QRect*[4];

    int starty = 60;
    int d = 10;
    int l = 120;

    for (int i = 0; i < 4; i++, starty += l + d){
        tile[i] = new QRect[4];

        for (int j = 0, startx = 50; j < 4; j++, startx += l + d){
            tile[i][j].setTopLeft(QPoint(startx, starty));
            tile[i][j].setBottomRight(QPoint(startx + l, starty + l));
        }
    }

    label_score = new QLabel(this);
    label_score->setGeometry(440, 0, 180, 60);
    QFont f;
    f.setPointSize(20);
    f.setBold(true);

    label_score->setFont(f);
    label_text = "Score: " + QString::number(grid->getScore());
    label_score->setText(label_text);

    button = new QPushButton(this);
    button->setGeometry(35,15,150,35);
    f.setPointSize(15);
    button->setFont(f);
    button->setText("New Game");

    connect(button, SIGNAL(clicked()), this, SLOT(newGame()));
}

void QGrid::newGame(){
    grid->reset();
    status = true;
    label_text = "Score: " + QString::number(grid->getScore());
    label_score->setText(label_text);
    update();
}

QGrid::~QGrid()
{
    delete grid;

    for (int i = 0; i < 4; i++)
        delete tile[i];

    delete label_score;
    delete button;
}

void QGrid::paintEvent(QPaintEvent *ev){
    if (status)
        label_text = QString::number(grid->getScore());

    this->setWindowTitle("2048 (" + label_text + ")");

    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing, true);

    QRect rBack(QPoint(40,50), QPoint(570,580));

    if (!status){
        QFont fgo;
        fgo.setPointSize(64);
        QPen pgo(QColor("#F24F0A"));
        p.setFont(fgo);
        p.setPen(pgo);
        p.drawText(rBack, Qt::AlignCenter, label_text);

        return;
    }

    QBrush bBack(_map[0].second);
    bBack.setStyle(Qt::SolidPattern);
    p.setBrush(bBack);
    p.drawRect(rBack);

    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            int tv = grid->getTileValue(i, j);

            QPen border(_map[0].second, 0);
            p.setPen(border);

            QBrush tileColor(_map[tv].first);
            tileColor.setStyle(Qt::SolidPattern);
            p.setBrush(tileColor);

            p.drawRect(tile[i][j]);

            QFont font;
            font.setPointSize(tv > 64 ? tv > 512 ? 24 : 30 : 36);
            font.setBold(true);
            p.setFont(font);


            QPen text(_map[tv].second);
            p.setPen(text);

            if (tv != 0)
            p.drawText(tile[i][j], Qt::AlignCenter, QString::number(tv));

            if (tv == 2048 && grid->getScore() >= 20000){
                status = false;
                label_text = "YOU WIN";

                update();
                return;
            }
        }
    }
}

void QGrid::keyPressEvent(QKeyEvent *ev){
    bool ret = true;
    bool ok = false;

    if (!status)
        return;

    switch (ev->key()) {
    case Qt::Key_Up :
        ret = grid->up();
        ok = true;
        break;

    case Qt::Key_Down :
        ret = grid->down();
        ok = true;
        break;

    case Qt::Key_Left :
        ret = grid->left();
        ok = true;
        break;

    case Qt::Key_Right :
         ret = grid->right();
         ok = true;
         break;

    }

    if (ok){
        update();
        label_text = "Score: ";
        label_text += QString::number(grid->getScore());

        label_score->setText(label_text);

        this->setWindowTitle("2048 (" + QString::number(grid->getScore()) + ")");
        if (!ret){
            if (!grid->gameStatus()){
                label_text = "GAME OVER";

                status = false;
            }
        }

    }
}

