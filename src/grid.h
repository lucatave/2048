/**
 * @file
 * @author Luca Tavernari
*/

#ifndef GRID_H
#define GRID_H

/**
 * Provides movement functionality along with the grid's status.
 */
class Grid
{
public:
    /**
     * Creates a new Grid.
     */
    Grid();

    /**
     * Destroys the istancied Grid.
     */
    ~Grid();

    /**
     * Resets the grid's status.
     */
    void reset();

    /**
     * Moves the tiles up
     *
     * @return true if a new element is spawned, false otherwise
     */
    bool up();

    /**
     * Moves the tiles down
     *
     * @return true if a new element is spawned, false otherwise
     */
    bool down();

    /**
     * Moves the tiles left
     *
     * @return true if a new element is spawned, false otherwise
     */
    bool left();

    /**
     * Moves the tiles right
     *
     * @return true if a new element is spawned, false otherwise
     */
    bool right();

    /**
     * Returns Grid's score
     *
     * @return Grid's score
     */
    int getScore();

    /**
     * Returns the value at the indicated row and column
     *
     *
     * @param i row
     * @param j column
     * @return value at the indicated rown and column
     */
    int getTileValue(int i, int j);

    /**
     * Checks if it's possible to make a move.
     *
     * @return true if it's possible a move, false otherwise.
     */
    bool gameStatus();

private:
    /**
     * Spawn a new element if it's possible
     *
     * @return true if a new element is spawned, false otherwise
     */
    bool spawnElement();

    /**
     * Generic function that moves the tiles in the specified "line" starting
     * from the tile identified by row and column.
     *
     * @param row indicates the row
     * @param col selects the element in the specified column from which the
     * move will start
     * @param selectorRC true if the interested line is a column,
     * false otherwise
     * @param inc
     * @param update
     * @return true if the move function make changes to the Grid,
     * false otherwise
     */
    bool move(int row, int col, bool selectorRC, int inc, bool update = true);

    /**
     * Generic function that merges the tiles in the specified the "line"
     * starting from the specified tile identified by row and column.
     *
     * @param row selects the row
     * @param col selects the element in the specified column from which the
     * merge will start
     * @param selectorRC true if the interested line is a column,
     * false otherwise
     * @param inc +1 if the movement if from top (or left), -1 otherwise
     * @param update true if the function modifies the grid, false otherwise
     * @return true if the merge function make changes to the Grid,
     * false otherwise
     */
    bool merge(int row, int col, bool selectorRC, int inc, bool update = true);

    /**
     * Generic function that iterates through the column, or the row using
     * both move and merge in the selected direction : <br/>
     * - up <br/>
     * - down <br/>
     * - left <br/>
     * - right <br/>
     *
     * @param row selects the row
     * @param col selects the element in the specified column from which the
     * iteration will start
     * @param selectorRC true if the interested line is a column,
     * false otherwise
     * @param inc +1 if the movement if from top (or left), -1 otherwise
     * @param update true if the function modifies the grid, false otherwise
     * @return true if the merge function make changes to the Grid,
     * false otherwise
     */
    bool iterateRC(int row, int col, bool selectorRC, int inc, bool update = true);

    int** m;
    int score;
};

#endif // GRID_H
