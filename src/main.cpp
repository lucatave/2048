/**
 * @mainpage 2048
 * This is a copy of the Gabriele Cirulli's 2048. It differs from his only in
 * the generation/spawn of a new element/tile in the Grid.
 *
 * The core functionality (backend) is implemented in the Grid class.
 * The frontend is implemented in the QGrid class.
 *
*/

#include "qgrid.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QGrid w;
    w.show();

    return a.exec();
}
