#-------------------------------------------------
#
# Project created by QtCreator 2014-07-31T23:18:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 2048
TEMPLATE = app


SOURCES += src/main.cpp\
        src/qgrid.cpp \
    src/grid.cpp

HEADERS  += src/qgrid.h \
    src/grid.h

QMAKE_MAC_SDK = macosx10.10
